from flask import Flask, jsonify, send_from_directory
from werkzeug.exceptions import default_exceptions
from werkzeug.exceptions import HTTPException
import subprocess as sp

__all__ = ['make_json_app']

def make_json_app(import_name, **kwargs):
    """ 
    Creates a JSON-oriented Flask app.

    All error responses that you don't specifically
    manage yourself will have application/json content
    type, and will contain JSON like this (just an example):

    { "message": "405: Method Not Allowed" }
    """
    def make_json_error(ex):
        response = jsonify(message=str(ex))
        response.status_code = (ex.code
                                if isinstance(ex, HTTPException)
                                else 500)
        return response

    app = Flask(import_name, **kwargs)

    for code in default_exceptions.iterkeys():
        app.error_handler_spec[None][code] = make_json_error

    return app 

app = make_json_app(__name__)

@app.route('/hello')
def hello():
    return jsonify(message="Hello world", status=0)

@app.route("/")
def refresh():
    try:
        output = sp.check_output("./refresh_certs.sh", stderr=sp.STDOUT, shell=True)
	response = jsonify(message=str(output), status=0)
	response.staus_code = 201
    except sp.CalledProcessError as e:
        error_code = e.returncode
        error_msg = e.output
	response = jsonify(message=str(error_msg), status=error_code)
	response.status_code = 500		
    return response

@app.route('/.well-known/acme-challenge/<challenge>')
def serve_acme(challenge):
    return send_from_directory('./challenges/.well-known/acme-challenge', challenge)

if __name__ == "__main__":
    app.run()
